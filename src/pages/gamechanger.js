import * as React from "react"
import { Flex, Box, Heading, Text, Link } from "@chakra-ui/react"
import GameChangerDemos from "../components/GameChangerDemos"

// markup
const GameChangerPage = () => {
  return (
    <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
        <Box mt='10' pt='5' borderTop='1px'>
          <Heading>
              What can GameChanger do?
          </Heading>
          <Text py='5'>
              Here is a simple donate link:{" "}
              <Link color='green' fontWeight='bold' href="https://testnet-wallet.gamechanger.finance/api/1/tx/woTCpHR5cGXConR4wqV0aXRsZcKwQURBIFNlbmRpbmcgRGVtb8KrZGVzY3JpcMSKb27DmgE_xJJHYW1lQ2hhxJllcsSTxKTEpnTEm8Sdby4gaHR0cHM6Ly9nxLBlY8S0xLZyLmbEmMS1Y2UvIMWAVGhlIG91xYTFnyBrZXkgYWxsb3dzIHnFniB0byDEocWSbsWcbXVsxIotYXNzZcS8dHLEtXNhY8SobsWdxZ9wxZ9zxYBJxolDYXJkxLXFssW9xb90xazGk8WcxbTEmGVkIGLFpWEgcG9saWN5ScahxLXGrsaJxpjGgE7FiywgZm_EuMSQxJJ3xZx1xb8gJ2HGlScgxJjGom90aCBjxb3EosWAUmVwbMaGxZzHg2RyxKLFv8WsxbHFpnZvacahZMSpYcSKxJkgxr_GtmLFoWlmxa3Fr8eNbsa2x6Nux6XFnHPFssa9x4zEtcWiZceSxqJ1aWzEl8enxrjEuMWudcKnxZ7FoMaawoHDmceUZMeYX3TEonQxcXJsMDd1OcW-ZHRkd3R6aHJ0MHpyyKs3OXllanhoZnZtc21qdDnIsXF5YXowa8WENnlkxblxxYI2cjl6NMiDMGrItjNhM2M3Z3c0NXUzMnZoYzJmyKFwyaLJinF2yYAyanfCkcKDwqhxdcS1xIrEgsKnMjDJuMm5wqjGp8apxqvGrcKjx4NhwqnGsnTGtMSxyoDGlQ">
              DONATE
              </Link>. You can generate links like this by clicking on "Generate Link" in the GameChanger Playground. The person accessing this link is prompted to connect to a GameChanger wallet.
          </Text>
        </Box>
        <Box mt='10' pt='5' borderTop='1px'>
          <Heading>
            TPBL Week 4 Task: Check out the GameChanger Playground
          </Heading>
          <Text py='5'>
            For details, <Link color='green' fontWeight='bold' href="https://gitlab.com/gimbalabs1/gimbal-token-pbl-experiment/-/blob/master/README.md">look at this README.md page</Link>.
          </Text>
        </Box>
        <Box mt='10' pt='5' borderTop='1px'>
          <Heading>
            GameChanger HTML Integration
          </Heading>
          <Text py='5'>
            (Incomplete)
          </Text>
          <GameChangerDemos />
        </Box>
    </Flex>
  )
}

export default GameChangerPage
