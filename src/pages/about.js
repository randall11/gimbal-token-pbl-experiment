import * as React from "react"
import { Link } from "gatsby"
import { Flex, Heading, Text } from "@chakra-ui/react"

// markup
const AboutPage = () => {
  return (
    <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
        <Heading>
            About Page
        </Heading>
        <Text py='10'>
            This template was made by the Gimbalabs PPBL team in November 2021 thru January 2022. It was initially used to build the Unsigs Marketplace, and now we are using it to run mini-experiments for Gimbal Token Project-Based Learning.
        </Text>
        <Heading>
            Next Steps
        </Heading>
        <Text py='10'>
            Test a simple task token, that requires AuthNFT to claim.
        </Text>
        <Text py='10'>
            Look at how to connect with CCVault and Flint wallets.
        </Text>

    </Flex>
  )
}

export default AboutPage
