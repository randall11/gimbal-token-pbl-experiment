import React, { useEffect, useState } from "react";
import { useStoreState } from "easy-peasy";

import Section from "../components/Section"
import Wallet from "../cardano/wallet";
import { fromAscii, fromHex } from "../utils/converter";
import { Flex, Heading, Text } from "@chakra-ui/react";

const IndexPage = () => {
  const connected = useStoreState((state) => state.connection.connected);
  const ownedTokens = useStoreState((state) => state.ownedTokens.policyIDS);
  const ownedUtxos = useStoreState((state) => state.ownedUtxos.utxos)
  const [ walletFunds, setWalletFunds ] = useState(null);

  useEffect(async () => {
    if (connected) {
      await Wallet.enable();
      const amt = await Wallet.getBalance();
      setWalletFunds(amt);
      console.log(amt)
    }
  }, [])

  return (
    <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
        <Heading>
            This is a template for interacting with Nami.
        </Heading>
        <Text py='10'>
            We can use this site to test simple smart contracts on testnet as we learn by doing.
        </Text>
        <Text>
          There are {ownedUtxos.length} UTxOs in your connected wallet.
        </Text>
    </Flex>
  )
}

export default IndexPage;
