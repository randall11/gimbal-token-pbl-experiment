import * as React from "react"
import { Link } from "gatsby"
import { Flex, Heading, Text } from "@chakra-ui/react"

// markup
const PublicPage = () => {
  return (
    <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
        <Heading>
            Public Page
        </Heading>
        <Text py='10'>
            This is the public page. Anyone can see it. To view the Private page, please connect a Wallet.
        </Text>
    </Flex>
  )
}

export default PublicPage
