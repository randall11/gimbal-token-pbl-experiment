import React, { useState, useEffect } from "react"
import Cardano from "../cardano/serialization-lib"
import Wallet from "../cardano/wallet"
import { serializeTxUnspentOutput, valueToAssets } from "../cardano/transaction"
// keeping for reference
// import { unsigPolicyId } from "../cardano/market-contract"
import { fromHex, toStr } from "../utils/converter"
import { useStoreActions, useStoreState } from "easy-peasy";

import { Box, Flex, Heading, Text } from "@chakra-ui/react"

import TaskList from "../components/TaskList"
import TokenDetails from "../components/TokenDetails"

// TODO: move first two functions to some utility? Wrap entire app in this?

async function getWalletUtxoValues() {
  await Cardano.load();
  const utxos = await Wallet.getUtxos();

  const utxoValues = utxos
    .map((utxo) => serializeTxUnspentOutput(utxo).output())
    .map((txOut) => valueToAssets(txOut.amount()))

  return [...new Set(utxoValues)];
};

async function getNativeAssets() {
  let utxoValueArray = await getWalletUtxoValues();
  let nativeAssets = [];

  console.log(utxoValueArray)

  utxoValueArray.forEach(utxo => {
    if (utxo.length > 1) {
      let substring = utxo[1].unit.substring(56);
      let name = toStr(fromHex(substring));
      let output = {
        assetName: name,
        quantity: utxo[1].quantity
      }
      console.log(output.assetName, output.quantity)
      nativeAssets.push(output)
    }
  });

  console.log(nativeAssets);
  return nativeAssets;
}

function countTaskTokens() {

}

// Todo: Asset List in store - should we use ownedTokens directly, or have another list of strings? - just names to check
// Try this: use what we have to check for an asset passed as param:
function getThisAsset(valuesArray, whichAsset) {
  for (let x in valuesArray) {
    const object = valuesArray[x]
    if (object.assetName == whichAsset) return object;
  }
  return {}
}

const PrivatePage = () => {
  const connected = useStoreState((state) => state.connection.connected);
  const [collection, setCollection] = useState([]);
  const ownedTokens = useStoreState((state) => state.ownedTokens.values);
  const setOwnedTokens = useStoreActions((actions) => actions.ownedTokens.add);
  const [hasAuthToken, setHasAuthToken] = useState({});
  const [hasTaskToken, setHasTaskToken] = useState({});
  const [quantityTaskTokens, setQuantityTaskTokens] = useState(0);




  useEffect(async () => {
    if (connected) {
      const tokens = await getNativeAssets();
      setCollection(tokens);
    }
  }, []);

  useEffect(() => {
    if (connected) {
      setOwnedTokens(collection);
    }
  }, [collection]);

  useEffect(() => {
    if (connected) {
      setHasAuthToken(getThisAsset(ownedTokens, "tpblTestAuth"));
      setHasTaskToken(getThisAsset(ownedTokens, "tpblTaskToken"));
    }
  }, [ownedTokens]);


  return (
    <Flex direction='column' w='90%' minH='400px' mx='auto' p='10'>
      <title>Private</title>
      <Box>
        <Heading>
          TPBL Token Playground
        </Heading>
        {connected ? (
          <Flex direction="column">
            <Text pt='3'>
              Nami Wallet is connected at address:
            </Text>
            <Text fontSize='xs' pb='3'>
              {connected}
            </Text>

            <Heading borderTop='1px' pt='3'>
              What tokens are you holding?
            </Heading>
            <Text>
              {JSON.stringify(collection)}
            </Text>
            <Text>
              If you have a tpblTestAuth token, you should see it here:
            </Text>
            <TokenDetails assetName={hasAuthToken.assetName} number={hasAuthToken.quantity} metadatakey='1' />

          </Flex>
        ) : (
          <div>
            <Text>
              To view your private page, please connect a wallet with this:
            </Text>
          </div>
        )}
      </Box>
      {
        hasAuthToken.assetName ?
          <TaskList /> : ("")
      }

    </Flex>
  )
}

export default PrivatePage

// Playing with Nami Wallet



//   const on = await enableWallet();
//   console.log("the wallet is on! ", on)
//   if(on) {
//     let assetList = await getOwnedAssets()
//     assetList.forEach(element => {
//       if (element.startsWith(unsigID))
//       {
//         let output = fromHex(element.substring(56))
//         console.log(toStr(output))
//       }

//     });
//     let encbal = await getUtxos()
//     encbal.forEach(element => {
//       console.log(element);
//       let bal = serializeTxUnspentOutput(element);
//       let assets = valueToAssets(bal.output().amount());
//       console.log(assets);
//     });

// }
// }
