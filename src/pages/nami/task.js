import React, { useState, useEffect } from "react"
// import Cardano from "../cardano/serialization-lib"
import Wallet from "../../cardano/wallet"
// import { serializeTxUnspentOutput, valueToAssets } from "../cardano/transaction"
// keeping for reference
// import { unsigPolicyId } from "../cardano/market-contract"
import { fromHex, toStr, fromBech32 } from "../../utils/converter"
import { useStoreActions, useStoreState } from "easy-peasy";

import { Box, Button, Flex, Heading, Text } from "@chakra-ui/react"

import { mintDummyTask } from "../../cardano/02-mint-dummy-task";


// markup
const TaskPage = () => {
    const connected = useStoreState((state) => state.connection.connected)
    const utxos = useStoreState((state) => state.ownedUtxos.utxos)
    const setWalletUtxos = useStoreActions((actions) => actions.ownedUtxos.add)

    useEffect(async () => {
        if (await Wallet.enable()) {
            const utxos = await Wallet.getUtxos();
            setWalletUtxos(utxos);
        }
    }, []);

    const handleMintDummyTask = async () => {
        console.log("click")
        const taskCreator = { "address": fromBech32(connected), "utxosParam": utxos }
        const txHash = await mintDummyTask(taskCreator)
        console.log(txHash)
    }


    return (
        <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
            <Heading>
                Lets mint some real tasks.
            </Heading>
            <Text py='10'>
                Components for PPBL Course
            </Text>
            <Box mt='10' pt='5' borderTop='1px'>
                <Heading>Mint a dummyTask token. Will work if user holds tpblTestAuth</Heading>
                <Text py='5'>Need proper success and error messages. Testing on console first.</Text>
                <Button onClick={handleMintDummyTask}>Mint a dummyTask token</Button>
            </Box>
            <Box mt='10' pt='5' borderTop='1px'>
                <Heading>Form to collect metadata and datum</Heading>
                <Text py='5'>Populate data objects for use in transaction builder.</Text>
                <Text py='5'>What goes on chain?</Text>
            </Box>
        </Flex>
    )
}

export default TaskPage