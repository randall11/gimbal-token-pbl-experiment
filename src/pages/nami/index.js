import React, { useState, useEffect } from "react"
// import Cardano from "../cardano/serialization-lib"
import Wallet from "../../cardano/wallet"
// import { serializeTxUnspentOutput, valueToAssets } from "../cardano/transaction"
// keeping for reference
// import { unsigPolicyId } from "../cardano/market-contract"
import { fromHex, toStr, fromBech32 } from "../../utils/converter"
import { useStoreActions, useStoreState } from "easy-peasy";

import { Box, Button, Flex, Heading, Text } from "@chakra-ui/react"

import { donate } from "../../cardano/01-donate-example";


// markup
const NamiPage = () => {
    const connected = useStoreState((state) => state.connection.connected)
    const utxos = useStoreState((state) => state.ownedUtxos.utxos)
    const setWalletUtxos = useStoreActions((actions) => actions.ownedUtxos.add)

    useEffect(async () => {
        if (await Wallet.enable()) {
            const utxos = await Wallet.getUtxos();
            setWalletUtxos(utxos);
        }
    }, []);

    const handleDonate = async () => {
        console.log("click")
        const donor = { "address": fromBech32(connected), "utxosParam": utxos }
        const txHash = await donate(donor)
        console.log(txHash)
    }


    return (
        <Flex direction='column' w='60%' minH='400px' mx='auto' p='10'>
            <Heading>
                Nami Playground
            </Heading>
            <Text py='10'>
                Components for PPBL Course
            </Text>
            <Box mt='10' pt='5' borderTop='1px'>
                <Heading>Donate Button</Heading>
                <Text py='5'>Practice with a simple transaction.</Text>
                <Text py='5'>{connected}</Text>
                <Text py='5'>{utxos}</Text>
                <Button onClick={handleDonate}>Press Me</Button>
            </Box>
            <Box mt='10' pt='5' borderTop='1px'>
                <Heading>Split Donate Button</Heading>
                <Text py='5'>See how to create a multi-output transaction</Text>
            </Box>
        </Flex>
    )
}

export default NamiPage