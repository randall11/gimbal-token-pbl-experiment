import Cardano from '../serialization-lib'
import {
    assetsToValue,
    createTxOutput,
    finalizeTx,
    initializeTx,
  } from "../transaction";
  import { fromHex } from "../../utils/converter";

// 02-MINT-DUMMY-TASK

export const mintDummyTask = async ({ address, utxosParam }) => {
    try {
        const { txBuilder, outputs } = initializeTx();
        const utxos = utxosParam.map((utxo) =>
            Cardano.Instance.TransactionUnspentOutput.from_bytes(fromHex(utxo))
        );

        // ppbl2
        const receiver = "addr_test1qzdgudzgf00ghdd8tw5ylylk4t76hsgm9pvr9ce73etppk0c8aursjfdhu7nr3sxujgczt2ndefwfc80pphdafv7fnrqact99a"
        const donationAmount = 5000000

        outputs.add(
            createTxOutput(
                Cardano.Instance.Address.from_bech32(receiver),
                assetsToValue([{ unit: "lovelace", quantity: `${donationAmount}`}])
            )
        );

        const txHash = await finalizeTx({
            txBuilder,
            utxos,
            outputs,
            changeAddress: address,
            minting: true,
        });
        console.log("SUCCESS")
        return {
            txHash,
        };
    } catch (error) {
        console.log(error, "in DONATE-EXAMPLE");
    }
};
