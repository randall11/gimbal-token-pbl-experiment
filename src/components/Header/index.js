import React from "react";
import { Link } from "gatsby"
import { useStoreState } from "easy-peasy";
import { Center, Heading, Text } from "@chakra-ui/react";
import WalletButton from "../WalletButton/WalletButton";

const Header = () => {
    const connected = useStoreState((state) => state.connection.connected);

    return (
        <div id="header">
            <Center bg='blue' color='white' py='10' gap='20'>
                <Text>
                    <Link to="/">Home</Link>
                </Text>
                <Text>
                    <Link to="/public/">Public</Link>
                </Text>
                <Text>
                    <Link to="/gamechanger/">GameChanger Testnet</Link>
                </Text>
                {connected ? (
                    <>
                        <Text>
                            <Link to="/private/">Private</Link>
                        </Text>
                        <Text>
                            <Link to="/nami/">Nami Playground</Link>
                        </Text>
                    </>
                ) : ""}

                <Text>
                    <Link to="/about/">About</Link>
                </Text>
                <WalletButton />
            </Center>
        </div>
    )
}

export default Header;