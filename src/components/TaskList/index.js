import React from "react";
import { Flex, Box, Heading, Text } from "@chakra-ui/react";
import Task from "../Task"
import GetAllTaskTokens from "../GetAllTaskTokens";

const TaskList = () => {
    return(
        <Box mt='10' pt='5' borderTop='1px'>
            <Heading>
                Task Token Experiment
            </Heading>
            <Heading size='md' py='5'>
                Gotta start somewhere...
            </Heading>
            <Text>
            If you hold an AuthNFT, you'll be able to see a task listing here. These tasks aren't really hidden. Anyone can look onchain (or in the source repo of this project!) and see them. When we mint Task Tokens, we can create our own proto-standards for holding relevant information in minting transaction metadata. This metadata could identify a task and a bounty, for example. We can hash the same data when we lock Task Tokens at a contract address. First, let's see if this works. Then, let's see what it inspires us to do.
            </Text>
            <Flex direction='row' wrap='wrap'>
                <GetAllTaskTokens />
            </Flex>
            <Heading size='md' py='5'>
                Try this!
            </Heading>
            <Text py='3'>
                Look for the link to this repository in Canvas, and check out the README.md for next steps.
            </Text>
            <Text py='3'>
                Note that there are no limits to access when we take this approach. This allows you to add tasks, but there is no protection for who can mint, or even which policyID will be in play. We also need to be able to match which token goes with which metadata. That is what plutus contracts are for!
            </Text>
        </Box>
    )
}

export default TaskList;