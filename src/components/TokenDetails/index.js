import React from "react";
import { Flex, Box, Heading, Text } from "@chakra-ui/react";

import AuthTokenMetadata from "../AuthTokenMetadata";

const TokenDetails = (props) => {
    const assetName = props.assetName
    const metadatakey = props.metadatakey
    const number = props.number

    return(
        <>
            { number ? (
                <Flex direction='row' w='400px' m='3' bg='blue' color='white'>
                    <Box p='3'>
                        { assetName ? (
                            <>
                                <Heading>
                                    {assetName}
                                </Heading>
                                <Heading pt='3' size='md'>
                                    quantity: {number}
                                </Heading>
                            </>
                        ) : (
                            ""
                        )}
                    </Box>
                    <Box ml='5' p='3' bg='orange' color='blue' justifySelf='flex-end'>
                        { (assetName && metadatakey) ? (
                            <AuthTokenMetadata metadatakey={metadatakey} />
                        ) : (
                            ""
                        )
                        }
                    </Box>
                </Flex>
            ) : (
                ""
            )

            }
        </>
    )
}

export default TokenDetails;