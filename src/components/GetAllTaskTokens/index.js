import React, {useEffect} from "react"
import { useQuery, gql } from '@apollo/client';
import { AlertDescription, Text, Heading, Flex, Box } from "@chakra-ui/react";

const METADATA_QUERY = gql`
query MetaTX {
  transactions(
      where: { metadata: { key: {_eq: "1618033"} } }
  ) {
      metadata {
          key
          value
      }
  }
}
`;

// What goes in the task metadata?
// name
// description
// call to action
// keep in mind that all of this can be automated

function GetTaskMetadata(data){
  const txArray = data.metadata;
  for (let x in txArray){
    if(txArray[x].key == "1618033"){
      return (txArray[x].value)
    }
  }
  return {};
}

function GetAllTaskTokens() {
  const { loading, error, data } = useQuery(METADATA_QUERY);
  console.log("my data", data);
  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error...</div>;

  const tasks = data.transactions.map(tx => GetTaskMetadata(tx))

  return(
    <>
      <Flex direction='row' wrap='wrap'>
        {tasks.map(task => (
          <Box key={task.task} m='5' p='5' bg='black' color='white'>
            <Heading>
              {task.task}
            </Heading>
            <Text py='1'>
              {task.description}
            </Text>
            <Text py='1'>
              gimbals: {task.gimbals}
            </Text>
            <Text py='1'>
              assignedBy: {task.assignedBy}
            </Text>
            <Text fontSize='xs' pt='2'>
              Created: {task.dateTaskCreated} | Due: {task.taskDueDate}
            </Text>
          </Box>
        ))}
      </Flex>

      {/* <Text fontSize='sm' py='3'>
        {data.transactions[0].metadata[1].value.name}
      </Text>
      <Text fontSize='sm' py='3'>
        {data.transactions[0].metadata[1].value.about}
      </Text>
      <Text fontSize='sm' py='3'>
        {data.transactions[0].metadata[1].value.issueDate}
      </Text> */}
    </>
  )
}


export default GetAllTaskTokens
