import React, {useEffect} from "react"
import { useQuery, gql } from '@apollo/client';
import { Text } from "@chakra-ui/react";

const METADATA_QUERY = gql`
query MetaTX {
  transactions(
      where: { metadata: { key: {_eq: "1618"} } }
  ) {
      metadata {
          key
          value
      }
  }
}
`;

function AuthTokenMetadata(props) {
  const { loading, error, data } = useQuery(METADATA_QUERY);
  console.log("my data", data);
  if (loading) return <div>Loading...</div>;
  if (error) return <div>Error...</div>;
  return(
    <>
      <Text fontSize='sm' py='3'>
        TOKEN NAME: {data.transactions[0].metadata[1].value.name}
      </Text>
      <Text fontSize='sm' py='3'>
        ABOUT: {data.transactions[0].metadata[1].value.about}
      </Text>
      <Text fontSize='sm' py='3'>
        ISSUED: {data.transactions[0].metadata[1].value.issueDate}
      </Text>
    </>

  )
}


export default AuthTokenMetadata
