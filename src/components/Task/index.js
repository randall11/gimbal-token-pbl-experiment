import React from "react";
import { Flex, Box, Heading, Text } from "@chakra-ui/react";

const Task = (props) => {
    return(
        <Box w='400px' h='125px' m='3' p='3' bg='red' color='white'>
            <Heading>
                {props.title}
            </Heading>
            <Text>
                 {props.text}
            </Text>
        </Box>
    )
}

export default Task;