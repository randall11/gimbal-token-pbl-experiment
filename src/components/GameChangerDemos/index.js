import * as React from "react"
import { Flex, Box, Heading, Text, Link } from "@chakra-ui/react"

const GameChangerDemos = () => {

    // const code={
    //     "type": "tx",
    //     "title": "ADA Sending Demo",
    //     "description": "A GameChanger Script Demo. https://gamechanger.finance/ . The output key allows you to define multi-asset transaction outputs. In Cardano assets are defined by a policyId and an assetName, for ADA we use 'ada' in both cases. Replace addresses to avoid donating us, but if you can, donate so we can keep building for you",
    //     "outputs": {
    //         "addr_test1qrl07u9ssdtdwtzhrt0zrrr79yejxhfvmsmjt9jxqyaz0ktp6ydulqht6r9z4ld0jms3a3c7gw45u32vhc2ftdp2f6rqvz02jw": [
    //             {
    //                 "quantity": "2000000",
    //                 "policyId": "ada",
    //                 "assetName": "ada"
    //             }
    //         ]
    //     }
    // };

    // If we use this in PPBL Course - what is the best pathway for adapting this?
    // window.onload=function() {
    //     //GameChanger Scripts are compressed using: https://www.npmjs.com/package/json-url
    //     const lib = JsonUrl('lzw'); // JsonUrl is added to the window object
    //     const link=document.getElementById("dapp-connector");

    //     lib.compress(code)
    //     .then(gcscript => {
    //         link.href="https://testnet-wallet.gamechanger.finance/api/1/tx/"+gcscript;
    //         link.innerHTML="<h2>▶️<br/>Run on GameChanger Wallet!</h2>"
    //     })
    //     .catch(err => {
    //         link.innerHTML="Error: " + err.message;
    //     })
    // };

    return (
        <>
            <h1>ADA Sending Demo</h1>
            <p>A GameChanger Script Demo. https://gamechanger.finance/ . The output key allows you to define multi-asset transaction outputs. In Cardano assets are defined by a policyId and an assetName, for ADA we use 'ada' in both cases. Replace addresses to avoid donating us, but if you can, donate so we can keep building for you</p>
            <p><a id="dapp-connector" href="#" >COMING SOON...</a></p>
            <br/><br/><br/>
            <p><i> 💪 Lets turn Cardano into the Blockchain of the Web! 💪 </i> <br/> <a target="_blanc" href="https://twitter.com/GameChangerOk"> Follow us on Twitter</a> </p>
            <p><i>Generated with ❤️ by <b> <a target="_blanc" href="https://gamechanger.finance/"> GameChanger Wallet Playground </a></b> 2021 </i></p>
        </>
    )
}

export default GameChangerDemos