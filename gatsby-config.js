const { ApolloLink } = require(`@apollo/client`)
const { createHttpLink } = require(`apollo-link-http`)
const { RetryLink } = require(`apollo-link-retry`)

const retryLink = new RetryLink({
  delay: {
    initial: 100,
    max: 2000,
    jitter: true,
  },
  attempts: {
    max: 5,
    retryIf: (error, operation) =>
      Boolean(error) && ![500, 400].includes(error.statusCode),
  },
})

module.exports = {
  siteMetadata: {
    siteUrl: "https://www.gimbalabs.com",
    title: "TPBL Allocation 0",
  },
  plugins: [
    "gatsby-plugin-styled-components",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    "@chakra-ui/gatsby-plugin",
    "gatsby-plugin-apollo",
  ],
};
