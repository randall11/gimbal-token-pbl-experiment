import React from "react";
import Layout from "./src/templates/layout";
import { ApolloProvider } from '@apollo/client';
import { client } from './src/apollo/client';
import { Buffer } from "buffer";
if (typeof window !== "undefined") window.Buffer = Buffer;

// Pass all props (hence the ...props) to the layout component so it has access to things like pageContext or location
const wrapPageElement = ({ element, props }) => (
  <ApolloProvider client={client}>
    <Layout {...props}>{element}</Layout>
  </ApolloProvider>
);

export default wrapPageElement;