import CustomLayout from "./wrapPageElement";
import "@fontsource/inter" // Defaults to weight 400.

export const wrapPageElement = CustomLayout;