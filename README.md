# Gimbal Token PBL

Testing simple smart contract interactions

## Week 4 PBL Task: Mint an insecure "tpblTaskToken", and include transaction metadata
1. Create or import a Testnet Wallet at [GameChanger Testnet](https://testnet-wallet.gamechanger.finance/welcome). Note: it is fine to use the same mnemonics in GameChanger that you are using with Nami -- this actually makes for a pretty convenient experience!

2. In GameChanger, on the left-side menu, choose Playground.

3. Replace the demo text with the following, and change the the metadata to match your task. Be sure to leave the key `1618033` unchanged.

```
{
    "type": "tx",
    "title": "Token Minting Demo",
    "description": "A GameChanger Script Demo. https://gamechanger.finance/ . To mint tokens you need monetary emission rules or 'scripts'. Here for adding public keys as whitnesses you can use accountIndex and addressIndex derivation. (0,0 is your address public key). Also you can define from an until when to mint with afterSlotOffset and beforeSlotOffset",
    "mints": [
        {
            "script": {
                "issuers": [
                    {
                        "accountIndex": 10,
                        "addressIndex": 100
                    }
                ],
                "afterSlotOffset": 0
            },
            "assets": [
                {
                    "assetName": "tpblTaskToken",
                    "quantity": "1"
                }
            ]
        }
    ],
    "metadata": {
        "1618033" : {
            "task": "Write the name of the task here",
            "description": "Write a description of the task, up to 64 characters in length",
            "assignedBy": "Write your name here",
            "dateTaskCreated": "YYYY-MM-DD",
            "taskDueDate": "YYYY-MM-DD",
            "gimbals": "integer"
        }
    }
}
```

4. If you used the same mnemonics in GameChanger and Nami, then you should see your new "tpblTaskToken" in your Nami wallet. If you used different mnemonics, then send the new token to the address you see in Nami.

5. On our test site, what do you see?



## GTPBL Testnet Addresses
Just for easy reference. Thanks for these!

If you haven't shared an address yet, please do so via [https://canvas.instructure.com/courses/4110570/assignments/27776288](https://canvas.instructure.com/courses/4110570/assignments/27776288) anytime.
```
addr_test1qr46xf8m582xr48anquuessxtnyh898uegzptvlh8y4cghezhmlrhqyl2h4vpphfy42ws9jf8urlfclv8sdsx0e6nvvq6jcf5d
addr_test1qzuwzwkwa3lr3mjawz0dzpnvd0zddkj9chtcnmzkwzl855lk7z0vd4ntcd00kh7ey6v6qflwejzeu2mfg9nue8hs74ks3ar9ma
addr_test1qzpyd44m0ucakhfvfkes8aq688qe80wuft4fwgtfvxnemkgk9v2xxpj4wmp5r4jcd0nj34zsznu57y4fxuy7yed7zmcqghhna3
addr_test1qqm9fwq5qkqagnhn0m7n5y29s38hzeuz9mpfez7vaczg6kw3ytz5sclwmn7wcfs0kkrd0twak4nhxxr0f5rzpvd849ds83lxue
addr_test1qzx3xd09g0relx654r70p7dwduqhdq0jlt8mwrxe0y0dehreglsjryy0003yh9enn29g3agxk2kfjduertrhakd75txseyzp0z
addr_test1qpruskpgfv7g0dw9zm3ggt07hsspz255eex8f62ugx5544j7w2t3626zkx0puy49dr8yxuk8zjdxk064crmr4npe4rysg3c96f
addr_test1qpdkpfydtz0j44npk4878zyjeggxen8y3j96sdgafas04unacy9e8u8wa72dp2q5lxa783pqt3v3qtnq86zt4kvj7svsyup6gd
addr_test1qp5qtywzqdeany7wa99fuzfhvy8eqsdee442t5zxktxe6eqml03rrjv6jrxqyfm5lzxj794uyl066t9pvmz049lueu7sja89k0
addr_test1qrcgatcs44wn9y045g8cwys7wt5q92fw3nz0n0nkssy06u02hlh9yu4r5t4ra3q3rzuhuh78l5kxxgewtnnsrhpycxyst99d7a
addr_test1qqe8xgwupe0xphmr0udglvlq2xc8tmkum0jkj9tym7k7cytp9kuws6zwgr834frvws9xc6dzad3t6wkehsmhxtmvx6uq9ltg2t
addr_test1qpap4rtun0vmgh2kmkneppe8l7kf2dwp95q72ewmujd0x8l5lw3mfyhsywtplrwd8g8qauan20x8cmvdzdq79vquhyyqpu4dlu
addr_test1qpx3hsy25cdn232dhv9yf5nnu6fty4mh6zkp0xr492lx7skrrcew8lm982yf4qw3qksn3zx28x5m4vcnl6xjldcgax6sd4fvtc
addr_test1qqc0wv8myc9w69ep4elv8446qq6sn0ydzfwah7famagultzxzfz3ff22segxgw2z2qcvlhff65pjrx9a40n0wtw0xq2qdfx3gy
addr_test1qq8ej0s53weutpvjanayhferx8k4tlu4ngv0v3faqdp098kyzn55q5v52x6hsr9s9raevacwnkklzc8yftaga8dtymaqcju220

```

## Transaction to send tpblTestAuth tokens
Here is the GameChanger script that was used to send tpblTestAuth tokens.

```
{
    "type": "tx",
    "title": "Multi-output Sending Demo",
    "description": "A GameChanger Script Demo. https://gamechanger.finance/ . With us, building complex transactions is easy. You can make payments to multiple addresses with multiple assets in the same transaction! How many blockchains can do that?. Replace policy ID for an existing one in your wallet. Replace addresses to try it out",
    "outputs": {
        "addr_test1qr46xf8m582xr48anquuessxtnyh898uegzptvlh8y4cghezhmlrhqyl2h4vpphfy42ws9jf8urlfclv8sdsx0e6nvvq6jcf5d": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qzuwzwkwa3lr3mjawz0dzpnvd0zddkj9chtcnmzkwzl855lk7z0vd4ntcd00kh7ey6v6qflwejzeu2mfg9nue8hs74ks3ar9ma": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qzpyd44m0ucakhfvfkes8aq688qe80wuft4fwgtfvxnemkgk9v2xxpj4wmp5r4jcd0nj34zsznu57y4fxuy7yed7zmcqghhna3": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qqm9fwq5qkqagnhn0m7n5y29s38hzeuz9mpfez7vaczg6kw3ytz5sclwmn7wcfs0kkrd0twak4nhxxr0f5rzpvd849ds83lxue": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qzx3xd09g0relx654r70p7dwduqhdq0jlt8mwrxe0y0dehreglsjryy0003yh9enn29g3agxk2kfjduertrhakd75txseyzp0z": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qpruskpgfv7g0dw9zm3ggt07hsspz255eex8f62ugx5544j7w2t3626zkx0puy49dr8yxuk8zjdxk064crmr4npe4rysg3c96f": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qpdkpfydtz0j44npk4878zyjeggxen8y3j96sdgafas04unacy9e8u8wa72dp2q5lxa783pqt3v3qtnq86zt4kvj7svsyup6gd": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qp5qtywzqdeany7wa99fuzfhvy8eqsdee442t5zxktxe6eqml03rrjv6jrxqyfm5lzxj794uyl066t9pvmz049lueu7sja89k0": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qrcgatcs44wn9y045g8cwys7wt5q92fw3nz0n0nkssy06u02hlh9yu4r5t4ra3q3rzuhuh78l5kxxgewtnnsrhpycxyst99d7a": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qqe8xgwupe0xphmr0udglvlq2xc8tmkum0jkj9tym7k7cytp9kuws6zwgr834frvws9xc6dzad3t6wkehsmhxtmvx6uq9ltg2t": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qpap4rtun0vmgh2kmkneppe8l7kf2dwp95q72ewmujd0x8l5lw3mfyhsywtplrwd8g8qauan20x8cmvdzdq79vquhyyqpu4dlu": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qpx3hsy25cdn232dhv9yf5nnu6fty4mh6zkp0xr492lx7skrrcew8lm982yf4qw3qksn3zx28x5m4vcnl6xjldcgax6sd4fvtc": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qqc0wv8myc9w69ep4elv8446qq6sn0ydzfwah7famagultzxzfz3ff22segxgw2z2qcvlhff65pjrx9a40n0wtw0xq2qdfx3gy": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ],
        "addr_test1qq8ej0s53weutpvjanayhferx8k4tlu4ngv0v3faqdp098kyzn55q5v52x6hsr9s9raevacwnkklzc8yftaga8dtymaqcju220": [
            {
                "quantity": "2000000",
                "policyId": "ada",
                "assetName": "ada"
            },
            {
                "quantity": "1",
                "policyId": "61c8081a9aed827437c927074efb9ac07310d050256e587fc8b8cd83",
                "assetName": "tpblTestAuth"
            }
        ]
    }
}
```
